#r "Newtonsoft.Json"

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Diagnostics;
using System.Net.Http.Headers;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Serilog;
using Serilog.Events;

public static void Run(TimerInfo myTimer, TraceWriter log)
{
	string authToken = "";
	var authApiEndpoint = GetEnvironmentVariable("AuthApiEndpoint");
	var accessKey = GetEnvironmentVariable("AccessKey");
	var secretKey = GetEnvironmentVariable("SecretKey");
	var logEntriesToken = GetEnvironmentVariable("LogEntriesToken");
	var env = GetEnvironmentVariable("Environment");
	var apiUrl = GetEnvironmentVariable("ApiUrl");

	var logger = new LoggerConfiguration()
		.WriteTo.Logentries(token: logEntriesToken, restrictedToMinimumLevel: LogEventLevel.Fatal)
		.CreateLogger();

	logger.Information($"Function - Scheduled recurring suggestions - Starting");

	try 
	{
		using (var client = new HttpClient())
		{
			var jsonHeader = new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json");
			client.DefaultRequestHeaders.Accept.Add(jsonHeader);

			var stringPayload = $"{{ \"userId\":\"{accessKey}\", \"privateKey\":\"{secretKey}\", \"scope\":\"DC\" }}";

			var content = new StringContent(stringPayload, Encoding.UTF8, "application/json");
			content.Headers.ContentType = jsonHeader;

			var response = client.PostAsync(authApiEndpoint, content).Result;
			var responseString = response.Content.ReadAsStringAsync().Result;

			authToken = JsonConvert.DeserializeAnonymousType(responseString, new { token = "" }).token;
		}

		using (var client = new HttpClient())
		{
			client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			client.Timeout = TimeSpan.FromMinutes(5);

			client.DefaultRequestHeaders.Add("me-environment", env);

			var response = client.PostAsync(apiUrl, new StringContent("{}", Encoding.UTF8, "application/json"));
			var responseString = response.Result.StatusCode;
		}
	}
	catch (Exception ex)
	{
		logger.Error(ex);
	}

	logger.Information($"Function - Scheduled recurring suggestions - Completed");
}

public static string GetEnvironmentVariable(string name)
{
	return System.Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process);
}